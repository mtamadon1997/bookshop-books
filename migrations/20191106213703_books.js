
exports.up = async function(knex) {
  await knex.schema.createTable('books', function (table) {
    table.increments('id')
    table.string('title').index().notNullable()
    table.string('publisher').index().notNullable()
    table.string('authors').index().notNullable()
  })
};

exports.down = function(knex) {
  knex.schema.dropTable('users')
};
