module.exports = {

  development: {
    client: 'sqlite3',
    connection: {
      filename: './db.sqlite3'
    }
  },

  staging: {
    client: 'sqlite3',
    connection: {
      filename: './db.sqlite3'
    }
  },

  production: {
    client: 'sqlite3',
    connection: {
      filename: './db.sqlite3'
    }
  }

}
