'user strict'
const Book = require('../models/Book')

class BookController {
  /**
   * Index and filter books
   *
   * @static
   * @param {*} ctx
   * @memberof BookController
   */
  static async index(ctx) {
    ctx.body = await Book.list(ctx.query, ctx.query.sort)
  }
  /**
   * Show one book by id
   *
   * @static
   * @param {*} ctx
   * @memberof BookController
   */
  static async show(ctx) {
    let book = await Book.findBy('id', ctx.params.id)
    if (!book) {
      ctx.status = 404
      ctx.body = {
        error: {
          message: 'item not found.'
        }
      }
      return
    }
    ctx.body = book
  }
  /**
   * Store one book
   *
   * @static
   * @param {*} ctx
   * @memberof BookController
   */
  static async store(ctx) {
    const check = await Book.validate(ctx.request.body)
    if (!check.status) {
      ctx.status = 400
      ctx.body = {
        error: check.error[0]
      }
      return
    }
    let book
    try {
      book = await Book.create(ctx.request.body)
    } catch (error) {
      ctx.status = 500
      ctx.body = {
        error: {
          message: 'Unexpected Server Error'
        }
      }
      return
    }
    ctx.status = 201
    ctx.body = book
  }
  /**
   * Update Book By Id
   *
   * @static
   * @param {*} ctx
   * @memberof BookController
   */
  static async update(ctx) {
    const check = await Book.validate(ctx.request.body)
    if (!check.status) {
      ctx.status = 400
      ctx.body = {
        error: check.error[0]
      }
      return
    }
    try {
      await Book.update({ id: ctx.params.id }, ctx.request.body)
    } catch (error) {
      ctx.status = 500
      ctx.body = {
        error: {
          message: 'Unexpected Server Error'
        }
      }
      return
    }
    ctx.body = {
      message: 'updated Successfully'
    }
  }
  /**
   * Delete Book By Id
   *
   * @static
   * @param {*} ctx
   * @memberof BookController
   */
  static async destroy(ctx) {
    try {
      await Book.deleteBy('id', ctx.params.id)
    } catch (error) {
      ctx.status = 500
      ctx.body = {
        error: {
          message: 'Unexpected Server Error'
        }
      }
      return
    }
    ctx.body = {
      message: 'deleted Successfully'
    }
  }
}
module.exports = BookController