'use strict' 

const debug = require('debug')('koa:bodyparser') 
const bodyParser = require('koa-bodyparser') 


/**
 * Return middleware that parses HTTP request body.
 *
 * @param {Object} [options={}] - Optional configuration.
 * @return {function} Koa middleware.
 * @throws {InvalidRequestBodyFormat} When failed to parse the request body.
 */
module.exports = (options = {}) => {
  debug('Create a middleware') 

  return bodyParser({
    ...options
  }) 
} 
