#!/usr/bin/env node

'use strict' 

const config = require('./config') 
const App = require('./app') 


const app = new App() 
app.listen(3000)

module.exports = app 
