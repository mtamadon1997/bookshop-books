'use strict'
const dotenv = require('dotenv')
dotenv.config()
const BaseModel = require('./BaseModel')
class User extends BaseModel{
  static get fields(){
    return {
      title: 'required|string',
      publisher: 'required|string',
      authors: 'required|array',
      'authors.*': 'required|string'
    }
  }
  static get tableName(){
    return 'books'
  }
  static async beforeCreate(data){
    data.authors = JSON.stringify(data.authors)
    return data
  }
  static async beforeUpdate(data){
    data.authors = JSON.stringify(data.authors)
    return data
  }
  static get filters(){
    return ['id','title','publisher','authors']
  }

}
module.exports = User