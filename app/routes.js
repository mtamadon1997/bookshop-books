'use strict' 

const Router = require('koa-router') 
const BookController = require('./controllers/BookController')
const router = new Router()

router.get('/api/v1/books', BookController.index)
router.get('/api/v1/books/:id', BookController.show)
router.post('/api/v1/books', BookController.store)
router.put('/api/v1/books/:id', BookController.update)
router.delete('/api/v1/books/:id', BookController.destroy)

module.exports = router 
